% -*- prolog -*-

fd_permutation(_,[]).
fd_permutation(L,[V|T]) :-
    fd_exactly(1,L,V),
    fd_permutation(L,T).

sudoku3x3(R1,R2,R3,R4,R5,R6,R7,R8,R9) :-
    R1=[A1,A2,A3,B1,B2,B3,C1,C2,C3],
    R2=[A4,A5,A6,B4,B5,B6,C4,C5,C6],
    R3=[A7,A8,A9,B7,B8,B9,C7,C8,C9],
    R4=[D1,D2,D3,E1,E2,E3,F1,F2,F3],
    R5=[D4,D5,D6,E4,E5,E6,F4,F5,F6],
    R6=[D7,D8,D9,E7,E8,E9,F7,F8,F9],
    R7=[G1,G2,G3,H1,H2,H3,I1,I2,I3],
    R8=[G4,G5,G6,H4,H5,H6,I4,I5,I6],
    R9=[G7,G8,G9,H7,H8,H9,I7,I8,I9],
    % each cell is a number from 1 to 9
    fd_domain([A1,A2,A3,A4,A5,A6,A7,A8,A9], 1, 9),
    fd_domain([B1,B2,B3,B4,B5,B6,B7,B8,B9], 1, 9),
    fd_domain([C1,C2,C3,C4,C5,C6,C7,C8,C9], 1, 9),
    fd_domain([D1,D2,D3,D4,D5,D6,D7,D8,D9], 1, 9),
    fd_domain([E1,E2,E3,E4,E5,E6,E7,E8,E9], 1, 9),
    fd_domain([F1,F2,F3,F4,F5,F6,F7,F8,F9], 1, 9),
    fd_domain([G1,G2,G3,G4,G5,G6,G7,G8,G9], 1, 9),
    fd_domain([H1,H2,H3,H4,H5,H6,H7,H8,H9], 1, 9),
    fd_domain([I1,I2,I3,I4,I5,I6,I7,I8,I9], 1, 9),
    AllDigits = [1,2,3,4,5,6,7,8,9],
    % column 1..9 constraints
    fd_permutation([A1,A4,A7,D1,D4,D7,G1,G4,G7], AllDigits),
    fd_permutation([A2,A5,A8,D2,D5,D8,G2,G5,G8], AllDigits),
    fd_permutation([A3,A6,A9,D3,D6,D9,G3,G6,G9], AllDigits),
    fd_permutation([B1,B4,B7,E1,E4,E7,H1,H4,H7], AllDigits),
    fd_permutation([B2,B5,B8,E2,E5,E8,H2,H5,H8], AllDigits),
    fd_permutation([B3,B6,B9,E3,E6,E9,H3,H6,H9], AllDigits),
    fd_permutation([C1,C4,C7,F1,F4,F7,I1,I4,I7], AllDigits),
    fd_permutation([C2,C5,C8,F2,F5,F8,I2,I5,I8], AllDigits),
    fd_permutation([C3,C6,C9,F3,F6,F9,I3,I6,I9], AllDigits),
    % row 1..9 constraints
    fd_permutation([A1,A2,A3,B1,B2,B3,C1,C2,C3], AllDigits),
    fd_permutation([A4,A5,A6,B4,B5,B6,C4,C5,C6], AllDigits),
    fd_permutation([A7,A8,A9,B7,B8,B9,C7,C8,C9], AllDigits),
    fd_permutation([D1,D2,D3,E1,E2,E3,F1,F2,F3], AllDigits),
    fd_permutation([D4,D5,D6,E4,E5,E6,F4,F5,F6], AllDigits),
    fd_permutation([D7,D8,D9,E7,E8,E9,F7,F8,F9], AllDigits),
    fd_permutation([G1,G2,G3,H1,H2,H3,I1,I2,I3], AllDigits),
    fd_permutation([G4,G5,G6,H4,H5,H6,I4,I5,I6], AllDigits),
    fd_permutation([G7,G8,G9,H7,H8,H9,I7,I8,I9], AllDigits),
    % sub-grid box 1..9 constraints
    fd_permutation([A1,A2,A3,A4,A5,A6,A7,A8,A9], AllDigits),
    fd_permutation([B1,B2,B3,B4,B5,B6,B7,B8,B9], AllDigits),
    fd_permutation([C1,C2,C3,C4,C5,C6,C7,C8,C9], AllDigits),
    fd_permutation([D1,D2,D3,D4,D5,D6,D7,D8,D9], AllDigits),
    fd_permutation([E1,E2,E3,E4,E5,E6,E7,E8,E9], AllDigits),
    fd_permutation([F1,F2,F3,F4,F5,F6,F7,F8,F9], AllDigits),
    fd_permutation([G1,G2,G3,G4,G5,G6,G7,G8,G9], AllDigits),
    fd_permutation([H1,H2,H3,H4,H5,H6,H7,H8,H9], AllDigits),
    fd_permutation([I1,I2,I3,I4,I5,I6,I7,I8,I9], AllDigits),
    % label all variables to instantiate one possibility
    fd_labeling([A1,A2,A3,A4,A5,A6,A7,A8,A9]),
    fd_labeling([B1,B2,B3,B4,B5,B6,B7,B8,B9]),
    fd_labeling([C1,C2,C3,C4,C5,C6,C7,C8,C9]),
    fd_labeling([D1,D2,D3,D4,D5,D6,D7,D8,D9]),
    fd_labeling([E1,E2,E3,E4,E5,E6,E7,E8,E9]),
    fd_labeling([F1,F2,F3,F4,F5,F6,F7,F8,F9]),
    fd_labeling([G1,G2,G3,G4,G5,G6,G7,G8,G9]),
    fd_labeling([H1,H2,H3,H4,H5,H6,H7,H8,H9]),
    fd_labeling([I1,I2,I3,I4,I5,I6,I7,I8,I9]).


% render the [x,y,...] rows for printing
portray2(X) :- var(X), write('_'). % otherwise it'd print _9834
portray2(X) :- write(X).
portray1([X]) :- portray2(X).
portray1([X|Y]) :- portray2(X), write(','), portray1(Y).
% (only suitable for list usage as below)
portray(X) :- write('['), portray1(X), write(']').

sudoku3x3print(R1,R2,R3,R4,R5,R6,R7,R8,R9) :-
    % print input rows, with blanks for unknown
    print(R1), nl, print(R2), nl, print(R3), nl, print(R4), nl,
    print(R5), nl, print(R6), nl, print(R7), nl, print(R8), nl,
    print(R9), nl,
    findall([R1,R2,R3,R4,R5,R6,R7,R8,R9],(
                sudoku3x3(R1,R2,R3,R4,R5,R6,R7,R8,R9),
                % print result rows, no more blanks!
                nl, print(R1), nl, print(R2), nl, print(R3), nl, print(R4), nl,
                print(R5), nl, print(R6), nl, print(R7), nl, print(R8), nl,
                print(R9), nl),
            _), nl, nl.

# gprolog-sudoku

A trivial implementation of sudoku in gnu-prolog's finite-domain
constraint solver facilities.  It nearly-instantly solves even
"very-difficult"-rated sudoku puzzles, all without having to hand-code
a search algorithm.

#

Sample puzzle:
    
    cat > PUZZLE.pro << EOF
    go :-
        sudoku3x3print([7,_,_,3,_,_,5,_,_],
                       [_,8,_,2,_,4,_,_,_],
                       [_,5,_,_,1,_,9,_,_],
                       [_,_,7,4,_,_,3,_,_],
                       [_,9,_,8,_,1,_,4,_],
                       [_,_,3,_,_,5,6,_,_],
                       [_,_,8,_,6,_,_,5,_],
                       [_,_,_,1,_,2,_,7,_],
                       [_,_,4,_,_,8,_,_,2]),
        halt.
    
    :- initialization(['sudoku.pro']).
    :- initialization(go).
    EOF

To solve:

    gprolog --consult-file PUZZLE.pro

Solution:

    GNU Prolog 1.4.4 (64 bits) [...]
    compiling [...]
    [7,_,_,3,_,_,5,_,_]
    [_,8,_,2,_,4,_,_,_]
    [_,5,_,_,1,_,9,_,_]
    [_,_,7,4,_,_,3,_,_]
    [_,9,_,8,_,1,_,4,_]
    [_,_,3,_,_,5,6,_,_]
    [_,_,8,_,6,_,_,5,_]
    [_,_,_,1,_,2,_,7,_]
    [_,_,4,_,_,8,_,_,2]
    
    [7,6,1,3,8,9,5,2,4]
    [3,8,9,2,5,4,7,6,1]
    [4,5,2,6,1,7,9,8,3]
    [8,1,7,4,2,6,3,9,5]
    [5,9,6,8,3,1,2,4,7]
    [2,4,3,9,7,5,6,1,8]
    [1,2,8,7,6,3,4,5,9]
    [9,3,5,1,4,2,8,7,6]
    [6,7,4,5,9,8,1,3,2]
    
